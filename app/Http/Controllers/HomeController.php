<?php

namespace App\Http\Controllers;
use com;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use Variant;

class HomeController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function DateTime(){

    }

    // view
    public function index()
    {
        return view('welcome');
    }

    public function journeyoperador()
    {
        $nome = '';
        $login = '';
        $online = '';

        if (isset($_GET['nome'])) {
            $nome = $_GET['nome'];
            $login = $_GET['login'];
            $online = $_GET['online'];
        } else {
            $nome = '';
        };

        $operacoes = 0;
        $minutes = 0;
        $dbDate = 0;
        $dbDate = new DateTime($online);
        $aDate = new DateTime(date('Y-m-d H:i:s'));
        $timePassed = $dbDate->diff($aDate);
        $minutes  = $timePassed->days * 24 * 60;
        $minutes += $timePassed->h * 60;

        $data = [
            'operacoes' => $this->operacoes($nome),
            'clientes' => $this->clientesOperador($nome),
            'login'=> $login,
            'nome' => $nome,
            'online'=> $online,
            'minutes'=> $minutes
        ];

        return view('/journey/operador', $data);
    }

    public function journey()
    {
        $clientes = 0;

        if (isset($_GET['ip'])) {
            $busca = $_GET['ip'];
            header("Location: /journey?ip=" . $busca);
        } else {
            $busca = '';
        };
        set_time_limit(0);

        $data = [
            'clientes' => $this->verificaTelefone($busca)
        ];


        return view('/journey/principal', $data);
    }

    function processospc(){

        $ip = $_GET['ip'];
        $processo = $this->processos($ip);
        $qtd = count($processo);

        $data = [
            'processo' => $processo,
            'ip' => $ip,
            'qtd' => $qtd
        ];
        return view('/journey/processospc', $data);
    }

    function servicospc(){
        $ip = $_GET['ip'];
        $servico = $this->servicos($ip);
        $qtd = count($servico);

        $data = [
            'ip' => $ip,
            'servico' => $servico,
            'qtd' => $qtd
            ];
        return view('/journey/servicospc', $data);
    }

    function campanhas(){

        $recebidas = $this->verificaRecebidas();

        foreach($recebidas as $recebida){
            $dbDate = new DateTime($recebida->data1);
            $aDate = new DateTime(date('Y-d-m H:i:s'));
            $timePassed = $dbDate->diff($aDate);
            $hora = $timePassed->d * 24 ;
            $min = ($timePassed->h + $hora) * 60 ;
            $segundos = (($timePassed->i + $min) * 60) + $timePassed->s  ;
        };

        $data = [
            'recebidas'   => $this->verificaRecebidas(),
            'enviadas'    => $this->verificaEnviadas(),
            'anexosMSSQL' => $anexosMSSQL = array(),
            'anexosMYSQL' => $anexosMYSQL = array(),
            'segundos' => $segundos
        ];


        return view('/journey/campanhas', $data);
    }

    //query
    function operacoes($nome)
    {
        $t_operacoes = "SELECT
			distinct(o.descricao)
		FROM skills a
			inner join operacoes o on o.id = a.id_operacao
			inner join usuarios u on u.idusuarios = a.id_operador
			where u.nome_sobrenome = '" . $nome . "'
		";

        $ret_op = DB::select(DB::raw($t_operacoes));
        return $ret_op;

    }

    function clientesOperador($nome)
    {
        $clientes_operador = "SELECT
			contato_id,
			nome_cliente,
			cpf_cnpj,
			criado_em,
			descricao,
			ultima_interacao_cliente,
			ultima_interacao_operador
		FROM chat_whatsapp a
			inner join operacoes o on o.id = a.id_operacao  
			inner join usuarios u on u.idusuarios = a.idOperador 
			where u.nome_sobrenome = '" . $nome . "' and  a.status = 1 
			or u.nome_sobrenome = '" . $nome . "' and a.status=3
			order by criado_em desc
		";

        $ret_co = DB::select(DB::raw($clientes_operador));
        return $ret_co;
    }

    //busca
    function verificaTelefone($busca)
    {
//        $result_query = array();
        $q_em_negociacao = "SELECT 
			w.contato_id as 'contato_do_cliente',
			w.nome_cliente as 'Nome',
			w.cpf_cnpj as 'cpf',
			t.descricao as 'Tipo_de_tratativa',
			s.descricao as 'Status',
			o.descricao as 'Operacao',
			u.email as 'Login',
			u.nome_sobrenome as 'Nome_Operador',
			u.ip_login as 'IP',
			w.criado_em as 'Criacao',
			w.ultima_interacao_cliente as 'iteração_do_cliente',
			w.ultima_interacao_operador as 'Iteração_do_operador',
			u.tempo_online as 'online'
			
		FROM 
			journey.chat_whatsapp w
			inner join tratativas_atendimentos  t on t.id = w.id_tratativa_atendimento
			right join usuarios u on u.idUsuarios = w.idOperador
			left join operacoes o on o.id = w.id_operacao
			left join status_journey s on s.id = w.status
		where 
			w.contato_id like '%" . $busca . "%' 
			or w.nome_cliente like '%" . $busca . "%' 
			or u.email like '%" . $busca . "%'
			or u.nome_sobrenome like '%" . $busca . "%'
			or u.ip_login like '%" . $busca . "%'
			order by w.id desc limit 15";

        $ret = DB::select(DB::raw($q_em_negociacao));
        return $ret;
    }

    // Informações da Máquina
    function infomaquina(){

        $ip = $_GET['ip'];
        set_time_limit(0);

        $disco = $this->discos($ip);
        $ram = $this->memRam($ip);


        $data = [
            'ip' => $ip,
            'processador' => $processador = $this->verificaCpuWin($ip),
            'total' => $disco[3],
            'livre' => $disco[5],
            'usada' => $disco[4],
            'totalR' => $ram[0],
            'livreR' => $ram[1],
            'usadaR' => $ram[2]
        ];
// 'total' => $total,
        //'livre' => $livre,
        //'usada' => $usada

        return view('/journey/infomaquina', $data);
    }

    function retorno(Request $request){

            $ip = $_GET['ip'];
            $disco = $this->discos($ip);
            $ram = $this->memRam($ip);

                $data = [
                    'total' => $disco[3],
                    'livre' => $disco[5],
                    'usada' => $disco[4],
                    'totalR' => $ram[0],
                    'livreR' => $ram[1],
                    'usadaR' => $ram[2],
                    'processador' => $this->verificaCpuWin($request->ip)
                ];

                return json_encode($data);
    }

    function retornocampanha(){

        $recebidas = $this->verificaRecebidas();
        $enviadas = $this->verificaEnviadas();

        $data = [
            'recebidas'   => $recebidas,
            'enviadas'  => $enviadas,
            'anexosMSSQL' => $anexosMSSQL = array(),
            'anexosMYSQL' => $anexosMYSQL = array()
        ];

        return json_encode($data);
    }


    // Array de dados da Máquina
    function verificaCpuWin($host){
        exec("wmic /node:".$host." cpu get loadpercentage", $cpu);
        Return $cpu[1];
    }

    function discos($host) {

        $result = array();
        $wmi = new COM("Winmgmts://".$host);
        $server = $wmi->execquery("SELECT * FROM Win32_LogicalDisk");

        foreach($server as $objItem){
            $total = (INT)($objItem->Size / 1073741824);
            $livre = (INT)($objItem->FreeSpace / 1073741824);
            $usada = $total- $livre;
            array_push($result,$objItem->Caption);
            array_push($result,$objItem->Description);
            array_push($result,$objItem->FileSystem);
            array_push($result,$total);
            array_push($result,$usada);
            array_push($result,$livre);

        }

        return $result;

    }

    function memRam($host) {

        $wmi = new COM("Winmgmts://".$host);
        $server = $wmi->execquery("SELECT * FROM Win32_OperatingSystem");
        $result = array();
        foreach($server as $objService){
            $total = (INT)($objService->TotalVisibleMemorySize / 1024);
            $livre = (INT)($objService->FreePhysicalMemory / 1024);
            $usada = $total- $livre;
            array_push($result,$total);
            array_push($result,$livre);
            array_push($result,$usada);
            array_push($result,$objService->TotalVirtualMemorySize);
            array_push($result,$objService->FreeVirtualMemory);


        }

        return $result;

    }

    function processos($host) {

        $get_sid = new Variant("", VT_BSTR);
        $strUserName = new Variant("", VT_BSTR);
        $strUserDomain = new Variant("", VT_BSTR);
        $usuario = "Sem acesso";
        $result = array();

        $wmi = new COM("Winmgmts://".$host);
        $server = $wmi->execquery("SELECT * from Win32_Process");

        foreach($server as $objItem){

            $resultado = $objItem->GetOwner($strUserName, $strUserDomain);

            if($resultado == 0){
                $usuario = "$strUserDomain\ $strUserName" ;

            }

            $ano = substr($objItem->CreationDate,0,4) ;
            $mes = substr($objItem->CreationDate,4,2) ;
            $dia = substr($objItem->CreationDate,6,2) ;
            $hora = substr($objItem->CreationDate,8,2);
            $min = substr($objItem->CreationDate,10,2);
            $seg = substr($objItem->CreationDate,12,2);

       array_push($result ,(array(
           $objItem->ProcessId,
           $objItem->ParentProcessId,
           $objItem->Description,
           $usuario ,
           round(($objItem->PageFileUsage/1024), 3)." MB" ,
           $dia."-".$mes."-".$ano." ".$hora.":".$min.":".$seg,
           $objItem->ExecutablePath
           )));
        }

        return $result;

    }

    function servicos($host) {

        $wmi = new COM("Winmgmts://".$host);
        $server = $wmi->execquery("SELECT * FROM Win32_Service");
        $result = array();
        foreach($server as $objService){
            array_push($result ,(array($objService->Name,$objService->State,$objService->StartMode,$objService->StartName)));
        }
        return $result;
    }

    //Campanhas
    function verificaRecebidas() {
        $sql = "select
					R.OPERACAO as 'OPERACAO',
					o.DESCRICAO as 'DESCRICAO',  
					count(R.OPERACAO) as 'Quantidade',
					(convert(datetime,min(DATA_HORA))) as 'data1'
				from MENSAGENS_RECEBIDAS R
				inner join operacoes o on o.CODIGO_OPERACAO = r.OPERACAO
				where nova = 1
				group by r.OPERACAO, o.DESCRICAO;";

        $ret = DB::connection('sqlsrv')->select(DB::raw($sql));
        //$ret = DB::select(DB::raw($sql));
        return $ret;
    }

    function verificaEnviadas() {
        $sql = "select
					R.CODIGO_OPERACAO as 'CODIGO_OPERACAO',
					o.DESCRICAO as 'DESCRICAO',  
					count(R.CODIGO_OPERACAO) as 'Quantidade',
					(select count(anexo) from retornos z where envio = 1 and anexo = 1 and Z.CODIGO_OPERACAO = R.CODIGO_OPERACAO group by CODIGO_OPERACAO) as 'anexos',
					min(DATA_HORA) as 'data'
				from RETORNOS R
				inner join operacoes o on o.CODIGO_OPERACAO = r.CODIGO_OPERACAO
				where envio = 1
				group by r.CODIGO_OPERACAO, o.DESCRICAO
				";

        $ret = DB::connection('sqlsrv')->select(DB::raw($sql));
        return $ret;
    }

    function verificaAnexos($id) {
        $sql = "SELECT 
					CONTATO,
					descricao,
					MENSAGEM_RETORNO,
					DATA_HORA
				FROM RETORNOS R
				inner join  operacoes o on o.CODIGO_OPERACAO = r.CODIGO_OPERACAO

				WHERE envio = 1 and ANEXO = 1 and r.CODIGO_OPERACAO =".$id."
				";

        $ret = DB::connection('sqlsrv')->select(DB::raw($sql));
        return $ret;
    }

    function verificaAnexoMYSQL($contato,$id) {

        $sql = mysqli_query("
		select 
				distinct(w.contato_id),
				w.nome_cliente,
				u.nome_sobrenome,
				o.descricao,
				w.ultima_interacao_operador,
				a.arquivo_anexo
		from journey.chat_whatsapp w
		inner join journey.chat_msg_whatsapp m on w.nome_cliente = m.para
		inner join journey.chat_msg_anexo a on a.chat_msg_id = m.id
		inner join journey.usuarios u on w.idoperador = u.idusuarios
		inner join journey.operacoes o on o.id = id_operacao
		where w.contato_id = ".$contato." and w.id_operacao = ".$id."
		;");
        print mysqli_error();


//        while ($dados = mysqli_fetch_assoc($sql)) {
//            array_push($result_query, $dados);
//        }


        $ret = DB::select(DB::raw($sql));
        return $ret;
    }

}
