@extends('layouts.app')
{{--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">--}}

@section('content')
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    SUPORTE
                </div>
                <p>CRC Sistemas</p>
                <div class="links">
                    <a href="/journey/principal">Journey</a>
                    <a href="/journey/campanhas">Campanhas</a>
                    <a href="#">CRC Digital</a>
                </div>
            </div>
        </div>
@endsection