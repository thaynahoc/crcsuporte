@extends('layouts.app')
{{--Refresh Automático--}}
{{--<meta http-equiv="refresh" content="2;url=/journey/infomaquina?ip={{$ip}}">--}}

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="jumbotron jumbotron-billboard">
                    <div class="text-center">
                        <h3>
                            <a href="/journey/principal"><i class="fa fa-angle-left fa-2x"></i></a>
                            Suporte Journey - Monitor
                        </h3>
                        <p>IP: {{$ip}}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row charts">
            <div class="col-sm-4 text-center">
                Memória RAM (Mb)
                <canvas id="chart-area-ram" class="chartjs-render-monitor"></canvas>
            </div>
            <div class="col-sm-4 text-center">
                Processador (%)
                <div id="chart-processador" style="height: 250px;"></div>
            </div>
            <div class="col-sm-4 text-center">
                Disco Rígido (Gb)
                <canvas id="chart-area-disco" class="chartjs-render-monitor"></canvas>
            </div>

        </div>
        <table class="table table-striped tablep">
            <thead>
            <tr class="bg-primary">
                <th colspan="3">Memória (Megabytes)</th>
                <th>Processador</th>
                <th colspan="3">Disco (Gigabytes)</th>
                <th>Processos</th>
                <th>Serviços</th>
            </tr>
            <tr>
                <th>Total</th>
                <th>Utilizada</th>
                <th>Livre</th>
                <th>%</th>
                <th>Total</th>
                <th>Utilizado</th>
                <th>Livre</th>
            </tr>
            </thead>

            <tbody class="dados" id="retorno_dados">
                <tr>
                    <td id="totalR">{{$totalR}}</td>
                    <td id="usadaR">{{$usadaR}}</td>
                    <td id="livreR">{{$livreR}}</td>
                    <td id="processador">{{$processador}}</td>
                    <td id="total">{{$total}}</td>
                    <td id="usada">{{$usada}}</td>
                    <td id="livre">{{$livre}}</td>
                    <td><a href="/journey/processos?ip={{$ip}}"><i class="fa fa-window-restore" aria-hidden="true"></i></a></td>
                    <td><a href="/journey/servicos?ip={{$ip}}"><i class="fa fa-server" aria-hidden="true"></i></a></td>
                </tr>
            </tbody>
        </table>

    </div>

@endsection

@section('scripts')

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>

    <script>
        refresh();

        var usadaRG;
        var livreRG;
        var usadaDisco;
        var livreDisco;
        var configdisco;
        var configram;
        var processadorR;

        setInterval(refresh, 5000);
        function refresh(){
            $.ajax({
                url: '/retorno?ip={{$ip}}',
                type: 'GET',
                async: false,
                success:function(data){
                    var ret = $.parseJSON(data);
                    $('#processador').html(ret.processador);
                    $('#totalR').html(ret.totalR);
                    $('#usadaR').html(ret.usadaR);
                    $('#livreR').html(ret.livreR);
                    $('#total').html(ret.total);
                    $('#usada').html(ret.usada);
                    $('#livre').html(ret.livre);
                    usadaRG = ret.usadaR;
                    livreRG = ret.livreR;
                    usadaDisco = ret.usada;
                    livreDisco = ret.livre;
                    processadorR = ret.processador;
                    atualizar_grafico();
                }
            });

        }


        Highcharts.chart('chart-processador', {
            chart: {
                type: 'spline',
                animation: Highcharts.svg, // don't animate in old IE
                marginRight: 10,
                events: {
                    load: function () {

                        // set up the updating of the chart each second
                        var series = this.series[0];
                        setInterval(function () {
                            var x = (new Date()).getTime(), // current time
                            y = parseInt(processadorR);
                            series.addPoint([x, y], true, true);
                        }, 5000);
                    }
                }
            },
            title: {
                text: ''
            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 150
            },
            yAxis: {
                title: {
                    text: ''
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                        Highcharts.numberFormat(this.y, 2);
                }
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: true
            },
            series: [{
                name: '',
                data: (function () {
                    // generate an array of random data
                    var data = [],
                        time = (new Date()).getTime(),
                        i;
                    for (i = -49; i <= 0; i += 1) {
                        data.push({
                            x: time + i * 1000,
                            y: parseInt(processadorR)
                        });
                    }
                    return data;
                }())
            }]
        });
        function atualizar_grafico() {
            configram = {
                type: 'pie',
                data: {
                    datasets: [{

                        data: [
                            usadaRG,
                            livreRG
                        ],
                        backgroundColor: [
                            window.chartColors.red,
                            window.chartColors.blue
                        ],
                        label: 'Dataset 1'
                    }],
                    labels: [
                        "Utilizada",
                        "Livre"
                    ]
                },
                options: {
                    responsive: true
                }
            };

            configdisco = {
                type: 'pie',
                data: {
                    datasets: [{

                        data: [
                            usadaDisco,
                            livreDisco
                        ],
                        backgroundColor: [
                            window.chartColors.red,
                            window.chartColors.blue
                        ],
                        label: 'Dataset 1'
                    }],
                    labels: [
                        "Utilizada",
                        "Livre"
                    ]
                },
                options: {
                    responsive: true
                }
            };

            var ctx = document.getElementById("chart-area-ram").getContext("2d");
            window.myPie = new Chart(ctx, configram);

            var ctx2 = document.getElementById("chart-area-disco").getContext("2d");
            window.myPie = new Chart(ctx2, configdisco);
        }
        var colorNames = Object.keys(window.chartColors);


    </script>
@endsection