@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="jumbotron jumbotron-billboard">
                    <div class="text-center">
                        <h3>Suporte Journey</h3>
                        <form class="form-inline buscarmenu">
                            <input class="form-control mr-sm-2" name="ip" id="ip" type="text" placeholder="Buscar por ... ">
                            <button class="btn btn-primary" type="submit" title="Buscar"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <table class="table table-hover table-condensed tablep">
            <thead>
            <tr class="bg-primary">
                <th class="text-center">Contato</th>
                <th class="text-center">Nome</th>
                <th class="text-center">Tipo de tratativa</th>
                <th class="text-center">Status</th>
                <th class="text-center">Operação</th>
                <th class="text-center">Matrícula Operador</th>
                <th class="text-center">Operador</th>
                <th class="text-center">IP</th>
            </tr>
            </thead>

            <tbody>
            @foreach($clientes as $cliente)
                <tr>
                    {{--{{  dd($clientes[0]) }}--}}
                    <td class= "text-center"> {{ $cliente->contato_do_cliente }} </td>
                    <td class= "text-center"> {{ $cliente->Nome }}               </td>
                    <td class= "text-center"> {{ $cliente->Tipo_de_tratativa }}  </td>
                    <td class= "text-center"> {{ $cliente->Status }}             </td>
                    <td class= "text-center"> {{ $cliente->Operacao }}           </td>
                    <td class= "text-center"> {{ $cliente->Login }}              </td>
                    <td class= "text-center">
                        <a href="/journey/operador?login={{$cliente->Login.'&nome='.$cliente->Nome_Operador.'&online='.$cliente->online }}">
                            {{ $cliente->Nome_Operador }}
                        </a>
                    </td>
                    <td class= "text-center">
                        <a href="/journey/infomaquina?ip={{$cliente->IP}}">
                            {{ $cliente->IP }}
                        </a>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
@endsection

