@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="jumbotron jumbotron-billboard">
                    <div class="text-center">
                        <h3>
                            <a href="/journey/principal"><i class="fa fa-angle-left fa-2x"></i></a>
                            Suporte Journey - Operador
                        </h3>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-condensed table-hover tablep" style="font-size: 14px !important;">

            {{--{{dd($minutes)}}--}}
            <h3 class="text-center @if($minutes <> 180) vermelho  @else verde @endif">
                <i class="fa fa-circle"></i>
                {{ $login }} - {{ $nome }}
                <span class="badge">Atende {{ count($clientes) }} Contatos</span>
            </h3>

            <p class="text-center @if($minutes <> 180) vermelho  @else verde @endif">
               Operações ||
                @foreach($operacoes as $operacao)
                    <span class="badge">
                        {{$operacao->descricao}}
                    </span>
                @endforeach
            </p>

            <thead>
            <tr class="bg-primary">
                <th class="text-center">Contato Cliente</th>
                <th class="text-center">Nome Cliente</th>
                <th class="text-center">CPF</th>
                <th class="text-center">Data de Criação</th>
                <th class="text-center">Operação</th>
            </tr>
            </thead>
            <tbody>
            @foreach($clientes as $cliente)
                @foreach($operacoes as $x)
                    @if($x->descricao == $cliente->descricao)
                        <tr>
                    @break
                    @else
                        <tr class="bg-vermelho">
                    @endif
                    @endforeach
                            <td class="text-center">{{$cliente->contato_id}}</td>
                            <td class="text-center">{{$cliente->nome_cliente}}</td>
                            <td class="text-center">@if (isset($cliente->cpf_cnpj)){{$cliente->cpf_cnpj}} @else <p>-----------</p> @endif</td>
                            <td class="text-center">{{$cliente->criado_em}}</td>
                            <td class="text-center">{{$cliente->descricao}}</td>
                        </tr>
            @endforeach
            </tbody>


        </table>
    </div>
@endsection
