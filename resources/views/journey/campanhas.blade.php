@extends('layouts.app')

@section('content')
    <input type="hidden" id="val_parc" value="0"/>
    <div class="container-fluid">
            <div class="row">
            <div class="col-md-12">
                <div class="jumbotron jumbotron-billboard">
                    <div class="text-center">
                        <h3>
                            Suporte Journey - Campanhas
                        </h3>
                    </div>
                </div>
            </div>
        </div>
            <div class="row">
                <div class="col-md-6 col-xs-6">
                    <h1 class="text-center">Whatsapp para Journey</h1>
                    <table class="table table-striped table-bordered table-responsive">
                        <thead>
                        <tr>
                            <th>COD</th>
                            <th>Operação</th>
                            <th>QTD MSG Retidas</th>
                            <th>Tempo</th>
                        </tr>
                        </thead>
                        <tbody id="recebidas">
                        @foreach($recebidas as $recebida)
                            <tr id="rec">
                                <td>{{$recebida->OPERACAO}}</td>
                                <td>{{$recebida->DESCRICAO}}</td>
                                <td>{{$recebida->Quantidade}}</td>
                                <td>{{$segundos}}</td>
                                @php $segundos = 0; @endphp
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6 col-xs-6">
                    <h1 class="text-center">Journey para whatsapp</h1>
                    <table class="table table-striped table-bordered table-responsive">
                        <thead>
                        <tr>
                            <th>COD</th>
                            <th>Operação</th>
                            <th>QTD MSG Retidas</th>
                        </tr>
                        </thead>
                        <tbody id="enviadas">
                        @foreach($enviadas as $enviada)
                            <tr id="env">
                                <td>{{$enviada->CODIGO_OPERACAO}}</td>
                                <td>{{$enviada->DESCRICAO}}</td>
                                <td>{{$enviada->Quantidade}}</td>
                                <td>{{$enviada->Quantidade}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
         <hr>
            <div class="row">
                <div class="col-md-6 col-xs-6">
                    <h1 class="page-header text-center">MY SQL</h1>
                    <table class="table table-striped table-bordered table-responsive">
                        <thead>
                        <tr>
                            <th>Contato</th>
                            <th>Nome do cliente</th>
                            <th>Operador</th>
                            <th>Operação</th>
                            <th>Data</th>
                            <th>Arquivo</th>
                        </tr>k
                        </thead>
                        <tbody id="anexomysql">
                        @foreach($anexosMYSQL as $anexos)
                            @foreach($anexos as $enviada)
                                <tr id="my">
                                    <td>{{$enviada['contato_id']}}</td>
                                    <td>{{$enviada['nome_cliente']}}</td>
                                    <td>{{$enviada['nome_sobrenome']}}</td>
                                    <td>{{$enviada['descricao']}}</td>
                                    <td>{{$enviada['ultima_interacao_operador']}}</td>
                                    <td>{{$enviada['arquivo_anexo']}}</td>
                                </tr>
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6 col-xs-6">
                    <h1 class="page-header text-center">MS SQL</h1>
                    <table class="table table-striped table-bordered table-responsive">
                        <thead>
                        <tr>
                            <th>Contato</th>
                            <th>Descrição</th>
                            <th>Caminho</th>
                            <th>Data</th>
                        </tr>
                        </thead>
                        <tbody id="anexomssql">
                        @foreach($anexosMSSQL as $anexos)
                            @foreach($anexos as $enviada)
                                <tr id="ms">
                                    <td>{{$enviada->CONTATO}}</td>
                                    <td>{{$enviada->descricao}}</td>
                                    <td>{{$enviada->MENSAGEM_RETORNO}}</td>
                                    <td>{{$enviada->DATA_HORA}}</td>
                                </tr>
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

    </div> <!--container-->
@endsection

@section('scripts')
    <script>

        refresh();
        setInterval(refresh, 5000);
        function refresh(){
            $.ajax({
                url: '/retornocampanha',
                type: 'GET',
                async: false,
                success:function(data){
                    var ret = $.parseJSON(data);

                    var enviadas = ret.enviadas;
                    var recebidas = ret.recebidas;
                    var anexomy = ret.anexosMSSQL;
                    var anexoms = ret.anexosMSSQL;

                    $('#rec').html();
                    $('#env').html();
                    $('#my').html();
                    $('#ms').html();

                    $.each(recebidas, function (k,v) {
                        ret += '<tr>';
                        ret += '<td id="rec-operacao">'+v.OPERACAO+'</td>';
                        ret += '<td id="rec-descricao">'+v.DESCRICAO+'</td>';
                        ret += '<td id="rec-quantidade">'+v.Quantidade+'</td>';
                        ret += '<td>{{$segundos}}</td>';
                        ret += '</tr>';
                        $('#recebidas').html(ret);
                    });

                    $.each(enviadas, function (k,v) {
                        ret += '<tr>';
                        ret += '<td id="env-operacao">'+v.CODIGO_OPERACAO+'</td>';
                        ret += '<td id="env-descricao">'+v.DESCRICAO+'</td>';
                        ret += '<td id="env-quantidade">'+v.Quantidade+'</td>';
                        ret += '</tr>';
                        $('#enviadas').html(ret);
                    });

                    $.each(anexomy, function (k,v) {
                        ret += '<tr>';
                        ret += '<td>'+v.contato_id+'</td>';
                        ret += '<td>'+v.nome_cliente+'</td>';
                        ret += '<td>'+v.nome_sobrenome+'</td>';
                        ret += '<td>'+v.descricao+'</td>';
                        ret += '<td>'+v.ultima_interacao_operador+'</td>';
                        ret += '<td>'+v.arquivo_anexo+'</td>';
                        ret += '</tr>';
                        $('#anexomysql').html(ret);
                        console.log(ret);
                    });

                    $.each(anexoms, function (k,v) {
                        ret += '<tr>';
                        ret += '<td>'+v.CONTATO+'</td>';
                        ret += '<td>'+v.descricao+'</td>';
                        ret += '<td>'+v.MENSAGEM_RETORNO+'</td>';
                        ret += '<td>'+v.DATA_HORA+'</td>';
                        ret += '</tr>';
                        $('#anexomssql').html(ret);
                        console.log(ret);
                    });
                }
            });

        }

    </script>
@endsection