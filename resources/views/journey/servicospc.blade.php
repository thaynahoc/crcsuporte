@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="jumbotron jumbotron-billboard">
                    <div class="text-center">
                        <h3>
                            <a href="/journey/infomaquina?ip={{$ip}}"><i class="fa fa-angle-left fa-2x"></i></a>
                            Suporte Journey - Monitor
                        </h3>
                        <p>IP: {{$ip}} - <small>{{$qtd}} serviços encontrados</small></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <table class="table table-striped tablep">
                <thead>
                <tr class="bg-primary">
                    <th>Nome</th>
                    <th>Status</th>
                    <th>Inicialização</th>
                    <th>Usuário</th>
                </tr>
                </thead>

                <tbody>
                @foreach($servico as $service)
                    <tr>
                    @foreach($service as $list)
                        <td>{{$list}}</td>
                    @endforeach
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection