<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/journey/principal',   'HomeController@journey');
Route::get('/journey/operador',    'HomeController@journeyoperador');
Route::get('/journey/infomaquina', 'HomeController@infomaquina');
Route::get('/journey/processos',   'HomeController@processospc');
Route::get('/journey/servicos',    'HomeController@servicospc');
Route::get('/journey/campanhas',   'HomeController@campanhas');


Route::get('/retorno', 'HomeController@retorno');
Route::get('/retornocampanha', 'HomeController@retornocampanha');

